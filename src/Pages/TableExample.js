import { Button } from "@mui/material";
import React, { useEffect, useState } from "react";
import { TableUI } from "../components/shared/Table";

const thead = [
  { key: "name", name: "Name", sortKey: "candidate.name" },
  { key: "species", name: "first Episode", sortKey: "candidate.code" },
  { key: "status", name: "Status", sortKey: "startDate" },
  { key: "type", name: "Type", sortKey: "area.name" },
  { key: "created", name: "Created", sortKey: "type.name" },
  { key: "imageA", name: "Img", sortKey: "level.name" },
  { key: "option", name: "Img", sortKey: "level.name" },
];

const data = (characters = []) =>
  characters.map((character) => ({
    ...character,
    name: character.name,
    species: character.species,
    status: character.status,
    type: character.type,
    created: character.created,
    imageA: (
      <img src={character.image} alt={character.name} width={30} height={30} />
    ),
    option: (
        <Button variant="contained">hello all</Button>
    )
  }));

export const TableExample = () => {
  const [characters, setCharacters] = useState([]);

  useEffect(() => {}, []);
  useEffect(() => {
    const getNames = async () => {
      const api = await fetch("https://rickandmortyapi.com/api/character");
      const res = await api.json();
      return res.results;
    };
    getNames()
      .then((v) => {
        setCharacters(data(v));
      })
      .catch(console.warn);
  }, []);

  return <TableUI data={characters} thead={thead} />;
};
