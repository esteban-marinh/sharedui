import React, { useEffect, useState } from "react";

import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import "./styles.css";
import { DatePicker, Input, Select, Toggle } from "../components";
import { Box, Button, Modal } from "@mui/material";

const locations = [
  { label: "Bogota", value: "bogota" },
  { label: "cali", value: "Cali" },
];
const toggles = [
  { label: "Yes", value: "yes" },
  { label: "No", value: "no" },
];

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};

const schema = (names = []) =>
  yup.object().shape({
    typeDocument: yup.string().required(),
    numDocument: yup.number().required(),
    firstName: yup.string().required(),
    firstSurname: yup.string().required(),
    middleName: yup.string().required(),
    secondSurname: yup.string().required(),
    dateBirth: yup.string().required(),
    dateIssueDocument: yup.string().required(),
    affiliationDate: yup.string().required(),
    typeAffiliation: yup.string().required(),
    documentNumber: yup.string().required(),
    administrativeAct: yup.string().required(),
    gender: yup.string().required(),
    filingDate: yup.string().required(),
  });

export const ReactFormExample = () => {
  const [isVisible, setIsVisible] = useState(false);
  const [names, setNames] = useState([]);
  const [character, setCharacter] = useState({
    name: '',
    image: '',
    created: ''
  });
  const handleOpen = () => setIsVisible(true);
  const handleClose = () => setIsVisible(false);
  const {
    control,
    handleSubmit,
    register,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    defaultValues: {
      typeDocument: "",
      numDocument: 0,
      dateIssueDocument: "",
      firstSurname: "",
      firstName: "",
      dateBirth: "",
      affiliationDate: "",
      typeAffiliation: "",
      documentNumber: "",
      administrativeAct: "",
      secondSurname: "",
      middleName: "",
      gender: "",
      filingDate: "",
    },
    resolver: yupResolver(schema(names)),
  });
  const onSubmit = (data) => console.log(data);

  useEffect(() => {
    const getNames = async () => {
      const api = await fetch("https://rickandmortyapi.com/api/character");
      const res = await api.json();
      return res.results;
    };
    getNames().then(setNames).catch(console.warn);
  }, []);

  return (
    <div className="App">
      <div className="App-title">
        New Affiliation Application - Subsidized Regime
      </div>
      <form className="App-form" onSubmit={handleSubmit(onSubmit)}>
        <Select
          label="Type Document"
          name="typeDocument"
          register={register}
          error={errors.typeDocument?.message}
          control={control}
          options={locations}
          onBlur={console.log}
        />
        <Input
          label="Num Document"
          name="numDocument"
          register={register}
          error={errors.numDocument?.message}
          control={control}
        />
        <DatePicker
          label="Date Issue Document"
          name="dateIssueDocument"
          register={register}
          error={errors.dateIssueDocument?.message}
          control={control}
        />
        <Toggle
          label="Administrative Act"
          name="administrativeAct"
          register={register}
          error={errors.administrativeAct?.message}
          control={control}
          options={toggles}
        />
        <Input
          label="First Surname"
          name="firstSurname"
          register={register}
          error={errors.firstSurname?.message}
          control={control}
        />
        <Input
          label="Second Surname"
          name="secondSurname"
          register={register}
          error={errors.secondSurname?.message}
          control={control}
        />
        <Input
          label="First Name"
          name="firstName"
          register={register}
          error={errors.firstName?.message}
          control={control}
          onBlur={(e) => {
            const data = names.find(name => name.name.toLowerCase().includes(e.target.value.toLowerCase()));
            if (!!data) {
              handleOpen();
              setCharacter(data);
            }
          }}
        />
        <Input
          label="Middle Name"
          name="middleName"
          register={register}
          error={errors.middleName?.message}
          control={control}
        />
        <DatePicker
          label="Date Birth"
          name="dateBirth"
          register={register}
          error={errors.dateBirth?.message}
          control={control}
        />
        <Toggle
          label="gender"
          name="gender"
          register={register}
          error={errors.gender?.message}
          control={control}
          options={toggles}
        />
        <DatePicker
          label="Affiliation Date"
          name="affiliationDate"
          register={register}
          error={errors.affiliationDate?.message}
          control={control}
        />
        <Select
          label="Type Affiliation"
          name="typeAffiliation"
          register={register}
          error={errors.typeAffiliation?.message}
          control={control}
          options={locations}
        />
        <Button type="submit" color="primary">
          Submit
        </Button>
      </form>
      <Modal
        open={isVisible}
        onClose={handleClose}
        aria-labelledby="parent-modal-title"
        aria-describedby="parent-modal-description"
      >
        <Box sx={{ ...style, width: 400 }}>
          <p>created: <b>{character.created}</b></p>
          <p>name: <b>{character.name}</b></p>
          <img src={character.image} alt={character.name} width={100} height={100} />
        </Box>
      </Modal>
    </div>
  );
};
