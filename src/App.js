import React from "react";

import { FormikExample, ReactFormExample, TableExample } from "./Pages";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";

const App = () => {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/formik">formik</Link>
            </li>
            <li>
              <Link to="/table">table</Link>
            </li>
            <li>
              <Link to="/react-hook-forms">react hook forms</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Routes>
          <Route path="/react-hook-forms" element={<ReactFormExample />} />
          <Route path="/table" element={<TableExample />} />
          <Route path="/formik" element={<FormikExample />} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
