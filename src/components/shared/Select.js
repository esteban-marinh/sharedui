import React from "react";
import PropTypes from "prop-types";

import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select as MaterialSelect,
} from "@mui/material";
import { Controller } from "react-hook-form";

export const Select = ({
  label,
  options = [],
  control = null,
  register = null,
  error = null,
  ...selectProps
}) => (
  <FormControl error={!!error} fullWidth>
    <InputLabel id="demo-simple-select-label">{label}</InputLabel>
    <Controller
      name="location"
      control={control}
      render={({ field }) => (
        <MaterialSelect label="location" {...field} {...selectProps}>
          {options.map((option) => (
            <MenuItem value={option.value}>{option.label}</MenuItem>
          ))}
        </MaterialSelect>
      )}
    />
    <FormHelperText id="component-error-text">{error}</FormHelperText>
  </FormControl>
);

Select.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  options: PropTypes.array.isRequired,
  register: PropTypes.func,
  error: PropTypes.object,
  visible: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
};
