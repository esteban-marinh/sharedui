import React from "react";
import { Controller } from "react-hook-form";
import PropTypes from "prop-types";

import {
  FormControl,
  FormHelperText,
  TextField,
} from "@mui/material";

export const DatePicker = ({
  name = "",
  label = "",
  register = null,
  error = "",
  control = null,
  ...inputProps
}) => (
  <FormControl error={!!error} variant="filled" fullWidth>
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <TextField
          id="date"
          label={label}
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          {...inputProps}
          {...field}
        />
      )}
    />
    <FormHelperText id="component-error-text">{error}</FormHelperText>
  </FormControl>
);

DatePicker.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  className: PropTypes.string,
  register: PropTypes.func.isRequired,
  error: PropTypes.object.isRequired,
  control: PropTypes.any.isRequired,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
};
