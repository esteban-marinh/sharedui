import React from "react";
import { Controller } from "react-hook-form";
import PropTypes from "prop-types";

import { FormControl, FormHelperText, TextField } from "@mui/material";

export const Input = ({
  label,
  type = "text",
  name = "",
  register = null,
  error = "",
  control = null,
  className = "",
  ...inputProps
}) => (
  <FormControl error={!!error} fullWidth>
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <TextField
          id="component-error"
          type={type}
          label={label}
          {...field}
          {...inputProps}
        />
      )}
    />
    <FormHelperText id="component-error-text">{error}</FormHelperText>
  </FormControl>
);

Input.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  label: PropTypes.string,
  prepend: PropTypes.string,
  className: PropTypes.string,
  register: PropTypes.func.isRequired,
  error: PropTypes.object.isRequired,
  control: PropTypes.any.isRequired,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
};
