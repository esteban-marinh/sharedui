import React from "react";
import PropTypes from "prop-types";
import { Controller } from "react-hook-form";

import {
  FormControl,
  FormHelperText,
  ToggleButton,
  ToggleButtonGroup,
} from "@mui/material";

export const Toggle = ({
  options = [],
  name = "",
  value,
  register = null,
  error = "",
  control = null,
  className = "",
}) => (
  <FormControl error={!!error} fullWidth>
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <ToggleButtonGroup
          color="primary"
          value={value}
          exclusive
          {...field}
        >
          {options.map((option) => (
            <ToggleButton value={option.value}>{option.label}</ToggleButton>
          ))}
        </ToggleButtonGroup>
      )}
    />
    <FormHelperText id="component-error-text">{error}</FormHelperText>
  </FormControl>
);

Toggle.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  type: PropTypes.string,
  label: PropTypes.string,
  prepend: PropTypes.string,
  className: PropTypes.string,
  register: PropTypes.func.isRequired,
  error: PropTypes.object.isRequired,
  control: PropTypes.any.isRequired,
};
