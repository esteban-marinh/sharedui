import React from 'react';
import PropTypes from 'prop-types';

export const TextArea = ({ label, register = null, error = null, medium = false, ...textareaProps }) => (
  <div className={['form-group', textareaProps.required ? 'required' : null].join(' ')}>
    {label && <label className="form-label">{label}</label>}
    <textarea
      className={['form-control', error ? 'is-invalid' : null].join(' ')}
      ref={register}
      rows={medium ? 3 : 5}
      maxLength={medium ? 500 : 10000}
      {...textareaProps}
    />
    {error && <small className="text-danger">{error.message}</small>}
  </div>
);

TextArea.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  medium: PropTypes.bool,
  register: PropTypes.func,
  error: PropTypes.object,
};

TextArea.displayName = 'TextArea';