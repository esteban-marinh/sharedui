import React from "react";
import PropTypes from "prop-types";

import { TableCell } from "@mui/material";

export const TableColumnUI = ({ rowValue, headValue, row, className }) => (
  <TableCell className={className}>
    <span className="text-cell">{rowValue}</span>
  </TableCell>
);

TableColumnUI.propTypes = {
  row: PropTypes.shape({
    className: PropTypes.string,
    id: PropTypes.string,
    actions: PropTypes.any,
  }),
  rowValue: PropTypes.any,
  headValue: PropTypes.any,
  className: PropTypes.string,
};
