import React from "react";
import PropTypes from "prop-types";

import { Table, TableHead, TableBody } from '@mui/material';
import { TableColumnUI, TableHeadUI, TableRowUI } from ".";


const TableUI = ({
  data,
  thead,
  children,
  selected,
  onSelect,
  classTableName = "",
}) => {
  return (
    <Table className={`table ${classTableName}`}>
      <TableHead>
        <TableHeadUI onSelect={onSelect} thead={thead} />
      </TableHead>
      <TableBody>
        {data.map((row, index) => (
          <TableRowUI key={index} row={row} selected={selected}>
            {thead.map((value, index) => (
              <TableColumnUI
                row={row}
                key={index}
                rowValue={row[value.key]}
                headValue={value.key}
                className={value.className || ""}
              />
            ))}
          </TableRowUI>
        ))}
      </TableBody>
      <tfoot>{children}</tfoot>
    </Table>
  );
};

TableUI.propTypes = {
  children: PropTypes.any,
  data: PropTypes.array,
  thead: PropTypes.array,
  selected: PropTypes.string,
  onSelect: PropTypes.func,
  classTableName: PropTypes.string,
};

export { TableUI };
