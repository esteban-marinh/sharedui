import React from "react";
import PropTypes from "prop-types";
import {TableRow, TableCell} from '@mui/material';

export const TableHeadUI = ({ thead, onSelect }) => (
  <TableRow>
    {onSelect ? <th></th> : null}
    {thead.map((value, index) => (
      <TableCell scope="col" key={index} className={value.className}>
        {value.name}
      </TableCell>
    ))}
  </TableRow>
);

TableHeadUI.propTypes = {
  thead: PropTypes.array,
  onSelect: PropTypes.func,
};
