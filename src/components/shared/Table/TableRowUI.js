import React, { useState } from "react";
import PropTypes from "prop-types";

import {TableRow, Collapse, Typography, TableCell} from '@mui/material';

export const TableRowUI = ({ row, selected, children }) => {
  const [open, setOpen] = useState(false)
  const handleClick = () => {
    console.log('Click');
    setOpen(open => !open);
  }
  
  return (
    <>
      <TableRow
      className={[
        row.className || "",
        selected && row.id === selected ? "bg-light" : null,
      ].join(" ")}
      onClick={handleClick}
      >
        {children}
      </TableRow>
      <TableRow >
        <TableCell colSpan={7}>
          <Collapse in={open} timeout="auto">
              <Typography>Expansion</Typography>
          </Collapse>
          </TableCell>
      </TableRow>
    
    </>
    
  );

}


TableRowUI.propTypes = {
  row: PropTypes.shape({
    className: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    id: PropTypes.string,
  }),
  selected: PropTypes.string,
  children: PropTypes.any,
};
