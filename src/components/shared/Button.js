import React from "react";
import PropTypes from "prop-types";
import { Button } from "@mui/material";

export const CustomButton = ({
  variant = "primary",
  icon = "",
  className = null,
  children,
  ...props
}) => {
  return (
    <Button type='button' {...props} className={className}>
      {icon && <i className="material-icons mr-3">{icon}</i>}
      {children}
    </Button>
  );
};

CustomButton.propTypes = {
  variant: PropTypes.string,
  icon: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.any.isRequired,
};
